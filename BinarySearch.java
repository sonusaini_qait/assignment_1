package assignment_1;

public class BinarySearch {
	
	
	public static void main(String ... a) {
		
		int [] inputArray = {10 , 11 , 13 , 14 , 15 , 20 } ;
		
		int leftIndex = 0 ;
		int rightIndex = inputArray.length ;
		
		int mid ;
		
		boolean isFound = false ;
		
		int searchedEle = 14  ;
		
		while (leftIndex <= rightIndex ) {
			
			mid = (leftIndex + rightIndex)/ 2 ;
			
			if(searchedEle == inputArray[mid]) {
				isFound = true ;
				break;
			}
			else if( searchedEle < inputArray[mid]) {
				 rightIndex = mid - 1 ;
			}
			else {
				leftIndex = mid + 1 ;
			}
			
			
		}
		
		if(isFound) {
			System.out.println("Element is found ");
		}
		else {
			System.out.println("Element is not found ");
		}
		
		
		
	}
	

}
